package com.dreamplug.booking.repository;

import com.dreamplug.booking.entry.Bookings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookingsDAO extends JpaRepository<Bookings, Long> {

        @Query(value = "select b.bid from bookings b where b.bdate = ?1 AND b.slotid = ?2 AND b.status = 1",nativeQuery = true)
        public Long getbookingid(LocalDate localDate,Long id);

        @Modifying
        @Transactional
        @Query(value = "update bookings set status = ?2 where bid = ?1 ", nativeQuery = true)
        public int updatebookingstatus(Long id, int status);

        @Query(value = "select * from bookings b where b.bdate = ?1 AND b.status = 1",nativeQuery = true)
        public List<Bookings> getallbookings(LocalDate localDate);

        @Modifying
        @Transactional
        @Query(value = "update bookings set bdate = ?1,slotid = ?2,status = ?3 where bid = ?4 ", nativeQuery = true)
        public int reschedulebooking(LocalDate ndate,Long nsid,int status,Long bid);

}
