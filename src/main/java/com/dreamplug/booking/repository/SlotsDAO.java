package com.dreamplug.booking.repository;

import com.dreamplug.booking.entry.slots;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface SlotsDAO extends JpaRepository<slots,Long> {

    @Query(value = "select s.status from slots s where s.slotid = ?1",nativeQuery = true)
    public Long getslotstatus(Long id);
}
