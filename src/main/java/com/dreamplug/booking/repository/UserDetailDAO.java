package com.dreamplug.booking.repository;

import com.dreamplug.booking.entry.userEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDetailDAO extends JpaRepository<userEntry, Long> {

}
