package com.dreamplug.booking.repository;

import com.dreamplug.booking.entry.DoctorEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorDetailDAO extends JpaRepository<DoctorEntry, Long> {
}
