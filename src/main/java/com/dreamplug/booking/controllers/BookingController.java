package com.dreamplug.booking.controllers;

import com.dreamplug.booking.entry.Bookings;
import com.dreamplug.booking.entry.DoctorEntry;
import com.dreamplug.booking.entry.userEntry;
import com.dreamplug.booking.services.manager.BookingsManger;
import com.dreamplug.booking.services.manager.DoctorDetailManager;
import com.dreamplug.booking.services.manager.UserDetailManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Slf4j
@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private UserDetailManager userDetailManager;

    @Autowired
    private DoctorDetailManager doctorDetailManager;

    @Autowired
    private BookingsManger bookingsManger;

    @PostMapping(path = "/adduser/",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public @ResponseBody
    ResponseEntity createUser(@RequestBody userEntry userEntry) {   //@Valid
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userDetailManager.create(userEntry));
    }

    @PostMapping(path = "/adddoctor/",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public @ResponseBody
    ResponseEntity createDoctor(@RequestBody DoctorEntry doctorEntry) {   //@Valid
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(doctorDetailManager.create(doctorEntry));
    }

    @PostMapping(path = "/addbooking/",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public @ResponseBody
    ResponseEntity createBooking(@RequestBody Bookings bookings) {   //@Valid
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(bookingsManger.createBooking(bookings));
    }

    @PatchMapping("/cancelbooking")
    public ResponseEntity cancelBooking(
            @RequestParam("bookingid") Long bid) {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(bookingsManger.cancelbooking(bid).get());
    };

    @GetMapping(path = "/getbookings")
    public @ResponseBody
    ResponseEntity getBookings(@RequestParam("qdate") String localDate) {   //@Valid
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(bookingsManger.getBookingsForTheDay(LocalDate.parse(localDate)));
    }

    @PatchMapping("/reschedule")
    public ResponseEntity rescheduleBooking(
            @RequestParam("bid") Long bid,@RequestParam("ndate") String newdate, @RequestParam("nsid") Long newsid) {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(bookingsManger.reschedulebooking(bid,LocalDate.parse(newdate),newsid));
    };








}
