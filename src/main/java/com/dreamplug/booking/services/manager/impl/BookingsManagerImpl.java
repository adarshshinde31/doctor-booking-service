package com.dreamplug.booking.services.manager.impl;

import com.dreamplug.booking.entry.Bookings;
import com.dreamplug.booking.repository.BookingsDAO;
import com.dreamplug.booking.repository.SlotsDAO;
import com.dreamplug.booking.services.manager.BookingsManger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class BookingsManagerImpl implements BookingsManger {

    @Autowired
    private BookingsDAO bookingsDAO;

    @Autowired
    private SlotsDAO slotsDAO;

    @Override
    public Bookings createBooking(Bookings bookings) {

        //first check if slotid is active then
        //check if already booked
        //then book it
        Long status = slotsDAO.getslotstatus(bookings.getSlotid());
        if(status == 1) {
            Long bid = bookingsDAO.getbookingid(bookings.getBdate(), bookings.getSlotid());
            if (bid == null)
                return bookingsDAO.save(bookings);
        }
        return null;
    }

    @Override
    public Optional<Bookings> cancelbooking(Long bid) {

        //Bookings bookings = bookingsDAO.getbookingstatus(bid,uid);
        Optional<Bookings> bookings = bookingsDAO.findById(bid);
        if (bookings != null) {
            //change status to 0
            int st = bookingsDAO.updatebookingstatus(bid,0);
            return bookingsDAO.findById(bid);
        }
            return bookings;
    }

    @Override
    public List<Bookings> getBookingsForTheDay(LocalDate localDate) {

        List<Bookings> bookings = bookingsDAO.getallbookings(localDate);
        return bookings;

    }

    @Override
    public Bookings reschedulebooking(Long bid, LocalDate localDate, Long sid) {
        //First check if bid is valid if yes
        //then check if the date is after/isequals
        //then check if > 4hrs
        //in my case slotid difference should be more than 4
        //slot id timing sid+8
        Optional<Bookings> bookings = bookingsDAO.findById(bid);
        LocalDate datetoday = LocalDate.now();

        if (bookings.isPresent()) {

                if(bookings.get().getBdate().isAfter(datetoday)){
                    bookingsDAO.reschedulebooking(localDate,sid,3,bid);
                }else if(bookings.get().getBdate().isEqual(datetoday)){

                    LocalTime slotTime = LocalTime.of(bookings.get().getSlotid().intValue()+8,00);
                    LocalTime currentTime = LocalTime.now();
                    long timetoslot = Duration.between(currentTime,slotTime).toHours();

                    if(timetoslot >= 4){
                        bookingsDAO.reschedulebooking(localDate,sid,3,bid);
                        LocalTime localTime = LocalTime.now();
                    }
                }

        }
        return null;
    }
}
