package com.dreamplug.booking.services.manager.impl;

import com.dreamplug.booking.entry.userEntry;
import com.dreamplug.booking.repository.UserDetailDAO;
import com.dreamplug.booking.services.manager.UserDetailManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDetailManagerImpl implements UserDetailManager {

    @Autowired
    private UserDetailDAO userDetailDAO;

    @Override
    public userEntry create(userEntry userEntry) {
        return userDetailDAO.save(userEntry);
    }
}
