package com.dreamplug.booking.services.manager;

import com.dreamplug.booking.entry.DoctorEntry;
import org.springframework.validation.annotation.Validated;

@Validated
public interface DoctorDetailManager {
    DoctorEntry create(DoctorEntry doctorEntry);
}
