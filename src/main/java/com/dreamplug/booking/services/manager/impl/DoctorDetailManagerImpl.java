package com.dreamplug.booking.services.manager.impl;

import com.dreamplug.booking.entry.DoctorEntry;
import com.dreamplug.booking.repository.DoctorDetailDAO;
import com.dreamplug.booking.services.manager.DoctorDetailManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoctorDetailManagerImpl implements DoctorDetailManager {

    @Autowired
    private DoctorDetailDAO doctorDetailDAO;

    @Override
    public DoctorEntry create(DoctorEntry doctorEntry) {
        return doctorDetailDAO.save(doctorEntry);
    }
}
