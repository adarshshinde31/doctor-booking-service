package com.dreamplug.booking.services.manager;


import com.dreamplug.booking.entry.Bookings;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BookingsManger {

    Bookings createBooking(Bookings bookings);

    Optional<Bookings> cancelbooking(Long bid);

    List<Bookings> getBookingsForTheDay(LocalDate localDate);

    Bookings reschedulebooking(Long bid, LocalDate localDate, Long sid);

}
