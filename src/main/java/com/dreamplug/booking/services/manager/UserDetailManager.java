package com.dreamplug.booking.services.manager;

import com.dreamplug.booking.entry.userEntry;
import org.springframework.validation.annotation.Validated;
//import javax.validation.Valid;

@Validated
public interface UserDetailManager {
    userEntry create(userEntry userEntry);  //@Valid
}
