package com.dreamplug.booking.entry;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "bookings")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Bookings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bid;

    private Long uid;

    private LocalDate bdate;

    private Long slotid;

    private String issue;

    private Long status;

}
