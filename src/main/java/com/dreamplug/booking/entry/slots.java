package com.dreamplug.booking.entry;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "slots")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class slots {

    @Id
    private Long slotid;
    private String stime;
    private String etime;
    private Long status;
}
